import 'package:get/get.dart';

import 'screens/chat/chat_binding.dart';
import 'screens/chat/chat_screen.dart';
import 'screens/chat_list/chat_list_binding.dart';
import 'screens/chat_list/chat_list_screen.dart';

///
/// All pages used in the application
/// Also linked to the relevant bindings in order to
/// initialize / dispose proper controllers when neccesarry
///

final pages = [
  GetPage(
    name: MyRoutes.chatListScreen,
    page: ChatListScreen.new,
    binding: ChatListBinding(),
  ),
  GetPage(
    name: MyRoutes.chatScreen,
    page: ChatScreen.new,
    binding: ChatBinding(),
  ),
];

/// All pages have their designated names which can be found here
class MyRoutes {
  static const chatListScreen = '/chat_list_screen';
  static const chatScreen = '/chat_screen';
}
