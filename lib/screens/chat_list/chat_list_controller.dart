import 'package:get/get.dart';

import '../../services/logger_service.dart';
import '../../services/matrix_service.dart';

class ChatListController extends GetxController {
  ///
  /// DEPENDENCIES
  ///

  final logger = Get.find<LoggerService>();
  final matrix = Get.find<MatrixService>();

  ///
  /// VARIABLES
  ///

  final peerList = ['@someplaceholderusername:matrix.org'];

  ///
  /// INIT
  ///

  @override
  Future<void> onInit() async {
    super.onInit();
  }

  ///
  /// METHODS
  ///

  void openChat(String peerId) {
    matrix.connectToChat(peerId);
  }
}
