import 'package:get/get.dart';

import 'chat_list_controller.dart';

class ChatListBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ChatListController.new);
  }
}
