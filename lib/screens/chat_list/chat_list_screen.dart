import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'chat_list_controller.dart';

class ChatListScreen extends GetView<ChatListController> {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text('appName'.tr),
        ),
        body: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 24.w,
              vertical: 40.h,
            ),
            child: ListView.builder(
              itemCount: controller.peerList.length,
              itemBuilder: (_, i) {
                final peerId = controller.peerList[i];
                return ListTile(
                  title: Text(peerId),
                  onTap: () => controller.openChat(peerId),
                );
              },
            ),
          ),
        ),
      );
}
