import 'package:get/get.dart';
import 'package:matrix/matrix.dart';

import '../../services/logger_service.dart';
import '../../services/matrix_service.dart';

class ChatController extends GetxController {
  final Room chatRoom;

  ChatController({
    required this.chatRoom,
  });

  ///
  /// DEPENDENCIES
  ///

  final logger = Get.find<LoggerService>();
  final matrix = Get.find<MatrixService>();

  ///
  /// INIT
  ///

  @override
  Future<void> onInit() async {
    super.onInit();
  }

  ///
  /// METHODS
  ///

  void closeChat() {
    matrix.closeActiveChat();
  }

  void sendTestMessage() {
    matrix.sendMessageToPeer('This is a test message');
  }
}
