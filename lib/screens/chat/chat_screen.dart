import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'chat_controller.dart';

class ChatScreen extends GetView<ChatController> {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text('appName'.tr),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: controller.closeChat,
          ),
        ),
        body: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 24.w,
              vertical: 40.h,
            ),
            child: Column(
              children: [
                const Text('Chat screen'),
                TextButton(
                  onPressed: controller.sendTestMessage,
                  child: const Text('Send test message'),
                ),
              ],
            ),
          ),
        ),
      );
}
