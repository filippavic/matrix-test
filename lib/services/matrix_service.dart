import 'dart:async';

import 'package:get/get.dart';
import 'package:matrix/matrix.dart';

import '../pages.dart';
import 'logger_service.dart';

class MatrixService extends GetxService {
  ///
  /// DEPENDENCIES
  ///

  final logger = Get.find<LoggerService>();

  ///
  /// VARIABLES
  ///

  final username = 'inchident';
  final password = '!bWC#Jg9BR';
  final homeserver = 'https://matrix.org';

  final _client = Rxn<Client>();
  Client? get client => _client.value;
  set client(Client? value) => _client.value;

  StreamSubscription<EventUpdate>? clientEventHandler;
  StreamSubscription<LoginState>? clientLoginStateHandler;

  final _activeChatRoom = Rxn<Room>();
  Room? get activeChatRoom => _activeChatRoom.value;
  set activeChatRoom(Room? value) => _activeChatRoom.value;

  ///
  /// INIT
  ///

  @override
  Future<void> onInit() async {
    super.onInit();

    await _createClient().then((_) => _attachEventHandlers());
  }

  ///
  /// CLOSE
  ///

  @override
  Future<void> onClose() async {
    await clientEventHandler?.cancel();
    await clientLoginStateHandler?.cancel();

    super.onClose();
  }

  ///
  /// METHODS
  ///

  Future<void> _createClient() async {
    client = Client('MatrixTest');

    if (client == null) {
      throw Exception('No client');
    }

    await client!.checkHomeserver(Uri.parse(homeserver));
    await client!.login(
      LoginType.mLoginPassword,
      identifier: AuthenticationUserIdentifier(user: username),
      password: password,
    );
  }

  void _attachEventHandlers() {
    if (client == null) {
      throw Exception('Cannot attach handlers if client is null');
    }
    clientEventHandler = client!.onEvent.stream.listen((eventUpdate) {
      print('New event update!');
    });

    clientLoginStateHandler =
        client!.onLoginStateChanged.stream.listen((state) {
      if (state == LoginState.loggedIn) {
        // Navigate to chats
      } else {
        // Navigate to login/register
      }
    });
  }

  Future<void> connectToChat(String peerId) async {
    if (client == null) {
      throw Exception('Cannot connect if client is null');
    }

    final chatRoomId = await client!.startDirectChat(peerId);

    final newRoom = client!.getRoomById(chatRoomId);

    if (newRoom != null) {
      activeChatRoom = newRoom;

      await Get.toNamed(MyRoutes.chatScreen, arguments: newRoom);
    } else {
      throw Exception('Room cannot be found');
    }
  }

  Future<void> closeActiveChat() async {
    activeChatRoom = null;

    Get.until((_) => Get.currentRoute == MyRoutes.chatListScreen);
  }

  Future<void> sendMessageToPeer(String message) async {
    if (activeChatRoom == null) {
      throw Exception('No active chat');
    }

    await activeChatRoom!.sendTextEvent(message);
  }

  void login(String emailAddress, String password) {
    if (client == null) {
      throw Exception('Cannot login if client is null');
    }

    final identifier = AuthenticationThirdPartyIdentifier(
        medium: 'email', address: emailAddress);

    client!.login(LoginType.mLoginPassword,
        identifier: identifier, password: password);
  }

  Future<void> register(
      String emailAddress, String username, String password) async {
    if (client == null) {
      throw Exception('Cannot login if client is null');
    }

    const name = 'Ime Prezime';

    final clientSecret = DateTime.now().millisecondsSinceEpoch.toString();

    await client!.requestTokenToRegisterEmail(clientSecret, emailAddress, 0);

    await client!.uiaRequestBackground((auth) =>
        client!.register(username: username, password: password, auth: auth));

    await client!.setDisplayName(username, name);
  }
}
